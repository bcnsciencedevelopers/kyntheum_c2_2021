var menuPrincipal;
var estructuraMenu;
var estructura;

function menu() {

    estructura = [];
    estructura[0] = ["ES_KYNTHEUM_2021_C2_00",  [
        "ES_KYNTHEUM_2021_C2_00_00", // Home
        "ES_KYNTHEUM_2021_C2_00_01", // Portada
        "ES_KYNTHEUM_2021_C2_00_02", // PASI100
        "ES_KYNTHEUM_2021_C2_00_03", // PASI100
        "ES_KYNTHEUM_2021_C2_00_04", // MOA
        "ES_KYNTHEUM_2021_C2_00_05", // MOA
        "ES_KYNTHEUM_2021_C2_00_06", // MOA
        "ES_KYNTHEUM_2021_C2_00_07", // MOA
        "ES_KYNTHEUM_2021_C2_00_08", // MOA
        "ES_KYNTHEUM_2021_C2_00_09", // MOA
        "ES_KYNTHEUM_2021_C2_00_10", // RAPIDEZ DE ACCION
        "ES_KYNTHEUM_2021_C2_00_11", // RAPIDEZ DE ACCION
        "ES_KYNTHEUM_2021_C2_00_12", // RAPIDEZ DE ACCION
        "ES_KYNTHEUM_2021_C2_00_13", // RAPIDEZ DE ACCION
        "ES_KYNTHEUM_2021_C2_00_14", // RAPIDEZ DE ACCION
        "ES_KYNTHEUM_2021_C2_00_15", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_16", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_17", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_18", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_19", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_20", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_21", // EFICACIA -> Aclaramiento
        "ES_KYNTHEUM_2021_C2_00_22", // EFICACIA -> RWE
        "ES_KYNTHEUM_2021_C2_00_23", // EFICACIA -> RWE
        "ES_KYNTHEUM_2021_C2_00_24", // EFICACIA -> RWE
        "ES_KYNTHEUM_2021_C2_00_25", // EFICACIA -> RWE
        "ES_KYNTHEUM_2021_C2_00_26", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_27", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_28", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_29", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_30", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_31", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_32", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_33", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_34", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_35", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_36", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_37", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_38", // EFICACIA -> CASOS
        "ES_KYNTHEUM_2021_C2_00_39", // EFICACIA -> Localizaciones
        "ES_KYNTHEUM_2021_C2_00_40", // EFICACIA -> Localizaciones
        "ES_KYNTHEUM_2021_C2_00_41", // EFICACIA -> Localizaciones
        "ES_KYNTHEUM_2021_C2_00_42", // EFICACIA -> Localizaciones
        "ES_KYNTHEUM_2021_C2_00_43", // EFICACIA -> Pacientes naive
        "ES_KYNTHEUM_2021_C2_00_44", // EFICACIA -> Pacientes naive
        "ES_KYNTHEUM_2021_C2_00_45", // EFICACIA -> Pacientes naive
        "ES_KYNTHEUM_2021_C2_00_46", // EFICACIA -> Pacientes naive
        "ES_KYNTHEUM_2021_C2_00_47", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_48", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_49", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_50", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_51", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_52", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_53", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_54", // EFICACIA -> Perfiles paciente
        "ES_KYNTHEUM_2021_C2_00_55", // SEGURIDAD
        "ES_KYNTHEUM_2021_C2_00_56", // SEGURIDAD
        "ES_KYNTHEUM_2021_C2_00_57", // RETRATAMIENTO
        "ES_KYNTHEUM_2021_C2_00_58", // RETRATAMIENTO
        "ES_KYNTHEUM_2021_C2_00_59", // RETRATAMIENTO
        "ES_KYNTHEUM_2021_C2_00_60", // POSOLOGIA
        "ES_KYNTHEUM_2021_C2_00_61", // RESUMEN


        
    ]];

    

    estructura[99] = ["ES_KYNTHEUM_2021_C2_FT",  [
        "ES_KYNTHEUM_2021_C2_FT_00"
    ]];

    estructuraMenu0 = [
        {
            "text": "<span class='menu-text'>Objetivo PASI100</span>", 
            "ic": "icon-1",
            "goto": {
                "slide": "2",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>MoA único</span>", 
            "ic": "icon-2",
            "goto": {
                "slide": "4",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>Rapidez de acción</span>", 
            "ic": "icon-3",
            "goto": {
                "slide": "10",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>Eficacia y RWE</span>", 
            "ic": "icon-4",
            "goto": {
                "slide": "11",
                "presentation": "0",
                "touch": true
            },
            // "children": false
            "children": [
                {
                    "text": "Aclaramiento completo mantenido",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "15",
                        "presentation": "0"
                    },
                    "children": false
                },
                {
                    "text": "RWE",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "22",
                        "presentation": "0"
                    },
                    "children": false
                },
                {
                    "text": "Casos clínicos",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "26",
                        "presentation": "0"
                    },
                    "children": false
                },
                {
                    "text": "Localizaciones especiales",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "39",
                        "presentation": "0"
                    },
                    "children": false
                },
                {
                    "text": "Paciente naïve",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "43",
                        "presentation": "0"
                    },
                    "children": false
                },
                {
                    "text": "Perfiles de pacientes",
                    "ic": "ic2",
                    "goto": {
                        "touch": true,
                        "slide": "47",
                        "presentation": "0"
                    },
                    "children": false
                }
            
        
            ]
        }, {
            "text": "<span class='menu-text'>Seguridad</span>", 
            "ic": "icon-5",
            "goto": {
                "slide": "55",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>Retratamiento <br> y vida media</span>", 
            "ic": "icon-6",
            "goto": {
                "slide": "57",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>Posología</span>", 
            "ic": "icon-7",
            "goto": {
                "slide": "60",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "<span class='menu-text'>Resumen</span>", 
            "ic": "icon-8",
            "goto": {
                "slide": "61",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];
    estructuraMenu1 = [
        {
            "text": "",
            "classes": "",
            "ic": "btn-home",
            "goto": {
                "slide": "0",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "text": "",
            "classes": "",
            "ic": "btn-instrucciones",
            "children": false
        }, {
            "text": "",
            "classes": "",
            "ic": "btn-referencias",
            "children": false
        }
    ];

    menuPrincipal0 = new Menu(estructuraMenu0, "mainMenu0");
    menuPrincipal1 = new Menu(estructuraMenu1, "menuTop");

    if (location.host === "localhost") {
        edetailing.enableTestMode();
    }
    inicializar_navigate();
}