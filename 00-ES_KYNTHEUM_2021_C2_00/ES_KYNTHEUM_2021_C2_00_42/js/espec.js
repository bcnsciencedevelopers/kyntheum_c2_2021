function espec() {
    // Inciar estructura
    var indexActius0 = { "nivel_1": "3", "nivel_2": "3", "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "42";

    // Para el nombre del account
    edetailing.getAccountName();

    // Especificaciones del slide
    $('#btn1, #btn2, #btn3, #btn4').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Quitar los active de todos los btns y los boxs
        $('.box, .btn').removeClass('active');
        $('.box').addClass('hidden');

        // Añadir active en el btn y en el box correspondiente
        $('#box' + lastChar).addClass('active');
        $('#btn' + lastChar).addClass('active');
        $('#box' + lastChar).removeClass('hidden');
    });

    rangeSlide();
}

function rangeSlide() {

    $( ".slider" ).slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: 100,
      value: 100,
      slide: function( event, ui ) {
          var caso = event.target.dataset['caso'];
          var valor = ui.value;
          valor = (valor - 100) * -1;
          $('#caso' + caso + ' .foto2').css({ 'height': +valor + '%' });
      }
    });
}