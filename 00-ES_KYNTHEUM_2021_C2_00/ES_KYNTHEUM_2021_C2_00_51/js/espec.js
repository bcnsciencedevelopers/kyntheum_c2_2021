function espec() {

    // Inciar estructura
    var indexActius0 = { "nivel_1": "3", "nivel_2": "5", "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "51";

    // Para el nombre del account
    edetailing.getAccountName();

    var mySwiper = new Swiper('.swiper-container', {
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

}
