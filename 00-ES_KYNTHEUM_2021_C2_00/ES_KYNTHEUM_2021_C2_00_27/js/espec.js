function espec() {

    // Inciar estructura
    var indexActius0 = { "nivel_1": "3", "nivel_2": "2", "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "27";

    // Para el nombre del account
    edetailing.getAccountName();

    // Especificaciones del slide

    /**
     * Cuando clicas una parte del cuerpo, se cambia la img
     */
    $('#btn1, #btn2').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Activar el botón clicado
        $('.btn').removeClass('active');
        $('#btn' + lastChar).addClass('active');

        // Quitar los active de todos los tabs
        $('.img').removeClass('active');
        $('.img').addClass('hidden');

        $('.img-' + lastChar).removeClass('hidden');
        $('.img-' + lastChar).addClass('active');
    });

    /**
     * Cunado clicas los tabs, se cambian las graficas.
     */
    $('#tab-1, #tab-2, #tab-3, #tab-4').on('click', function () {

        // saber que hemos clicado
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];
        // Quitar los active de todos los tabs
        $('.tab').removeClass('active');
        $('.grafica').removeClass('active');
        // Añadir el active al clicado
        $('#tab-' + lastChar).addClass('active');
        $('.grafica').addClass('hidden');
        $('#grafica-' + lastChar).removeClass('hidden');
        if (lastChar == 4) {
            $('.text-referencias').html('<b>PASI</b>: Psoriasis Area and Severity Index; <b>BSA</b>: Body Surface Area; <b>EVA</b>: escala analógica visual; <b>DLQI</b>: Dermatology Life Quality Index. <br/> *Debido a la discapacidad, las preguntas 3, 5, 6 y 9 carecían de relevancia; por ello, el valor máximo era 18. <br/> Cortesía del Dr. Ralf von Kiedrowski.');
        } else {
            $('.text-referencias').html('<b>PASI</b>: Psoriasis Area and Severity Index; <b>BSA</b>: Body Surface Area; <b>EVA</b>: escala analógica visual; <b>DLQI</b>: Dermatology Life Quality Index. <br/> Cortesía del Dr. Ralf von Kiedrowski.');
        }
    });

    // /**
    //  * Cuando clicas una parte del cuerpo, se cambia la img
    //  */
    // $('.img').on('click', function () {
    //     var id = $(this).attr('id');
    //     var lastChar = id[id.length - 1];

    //     // Popup
    //     $("#" + id).attr('onclick', 'openPop(\"popBig\")');
    //     $("#overlay").addClass('active');
    //     $("#overlay").css('z-index', '99');

    //     $("#global").append("<img src='css/img/casos_clinicos/1/img-" + lastChar + "-big.png' class='imgpopupBig' id='popBig'>");

    //     $('#popBig').on('click', function () {
    //         $("#overlay").css('z-index', '-199');
    //         $("#overlay").removeClass('active');
    //         $("#popBig").remove();
    //     });
    // });

    // $('#menuTop ul li:last-child span span').addClass('disabled');


}