function espec() {

    // Inciar estructura
    var indexActius0 = { "nivel_1": "3", "nivel_2": "5", "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "54";

    // Para el nombre del account
    edetailing.getAccountName();

    // Especificaciones del slide
    $('#btn6, #btn7, #btn8').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Quitar los active de todos los btns y los boxs
        $('.box, .btn').removeClass('active');
        $('.box').addClass('hidden');

        // Añadir active en el btn y en el box correspondiente
        $('#box' + lastChar).addClass('active');
        $('#btn' + lastChar).addClass('active');
        $('#box' + lastChar).removeClass('hidden');

        $('.subtext').addClass('hidden');
        $('#box' + lastChar+ '.subtext').addClass('active');
        

        // Modificamos el bg general
        $('#global').removeClass('global6');
        $('#global').removeClass('global7');
        $('#global').removeClass('global8');
        $('#global').addClass('global' + lastChar);
    });

    var mySwiper = new Swiper('.swiper-container', {
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    
    rangeSlide();
}

function rangeSlide() {

    $( ".slider" ).slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: 100,
      value: 100,
      slide: function( event, ui ) {
          var caso = event.target.dataset['caso'];
          var valor = ui.value;
          valor = (valor - 100) * -1;
          $('#caso' + caso + ' .foto2').css({ 'height': +valor + '%' });
      }
    });
}