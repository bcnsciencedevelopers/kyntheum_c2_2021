function espec() {

    // Inciar estructura
    var indexActius0 = { "nivel_1": "3", "nivel_2": "2", "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "29";

    // Para el nombre del account
    edetailing.getAccountName();

    // Especificaciones del slide
    var auxiliar = 1;
    /**
     * Cuando clicas una parte del cuerpo, se cambia la img
     */
    $('#btn1, #btn2, #btn3, #btn4').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Activar el botón clicado
        $('.btn').removeClass('active');
        $('#btn' + lastChar).addClass('active');

        // Quitar los active de todos los tabs
        $('.img').removeClass('active');
        $('.img').addClass('hidden');

        $('.img-' + lastChar).removeClass('hidden');
        $('.img-' + lastChar).addClass('active');

        $('#global').removeClass('bg' + auxiliar);
        $('#global').addClass('bg' + lastChar);

        auxiliar = lastChar;
    });

    /**
     * Cunado clicas los tabs, se cambian las graficas.
     */
    $('#tab-1, #tab-2, #tab-3, #tab-4').on('click', function () {
        // saber que hemos clicado
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];
        // Quitar los active de todos los tabs
        $('.tab').removeClass('active');
        $('.grafica').removeClass('active');
        // Añadir el active al clicado
        $('#tab-' + lastChar).addClass('active');
        $('.grafica').addClass('hidden');
        $('#grafica-' + lastChar).removeClass('hidden');
    });
}
