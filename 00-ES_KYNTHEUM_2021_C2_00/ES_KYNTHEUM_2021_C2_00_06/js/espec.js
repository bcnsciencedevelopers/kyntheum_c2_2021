function espec() {

    var indexActius0 = { "nivel_1": "1", "nivel_2": false, "nivel_3": false };
    menuPrincipal0.init(indexActius0);
    var indexActius1 = { "nivel_1": false, "nivel_2": false, "nivel_3": false };
    menuPrincipal1.init(indexActius1);
    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "6";
    edetailing.index_slide_actual = "1";

    // Para el nombre del account
    edetailing.getAccountName();

    // Especificaciones del slide
    $('#btn1, #btn2, #btn3').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Quitar los active de todos los btns y los boxs
        $('.box, .btn').removeClass('active');
        $('.box').addClass('hidden');

        // Añadir active en el btn y en el box correspondiente
        $('#box' + lastChar).addClass('active');
        $('#btn' + lastChar).addClass('active');
        $('#box' + lastChar).removeClass('hidden');

        // Borrar los checkbox si hemos clicado la 2 o la 3
        if (lastChar != 1) {
            $('#contentTab1').addClass('hidden');
        } else {
            $('#contentTab1').removeClass('hidden');
        }

    });

    $('#btnCheck1, #btnCheck2, #btnCheck3, #btnCheck4').on('click', function () {
        var id = $(this).attr('id');
        var lastChar = id[id.length - 1];

        // Cambiar la el texto y el checkbox
        $('.check span, .check p').removeClass('active');
        $('#btnCheck' + lastChar + ' span').addClass('active');
        $('#btnCheck' + lastChar + ' p').addClass('active');

        // Cambiar la imagen al pulsar el check
        $('.boxBg').addClass('hidden');
        $('#boxBg' + lastChar).removeClass('hidden');
    });

    // Especificaciones del slide
    $('#btn4').on('click', function () {
        // Hacer el popup con el fondo transparente
        $('.retina .popup, .swiper-slide').addClass('transparent');
    });

}
